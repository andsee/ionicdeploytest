import { Injectable } from '@angular/core';
import { Pro } from '@ionic/pro';
import { Platform } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class VersionService {

  binaryNoWait:string = 'Unknown version';
  //versionGetAppReady:string = 'Unknown version';
  buildID:string='Unknown';
  binaryVersion:string='Unknown';

  constructor(private platform: Platform) 
  { 
    // This fails even when injected
    this.getVersion().then((c:ICurrentConfig) =>
    {
      if(c)
      {
        this.binaryNoWait = c.binaryVersion;
      }
    });
    /*
    This fails as getApp() is undefined.
    Pro.getApp().platformReady().then(() =>
    {
      this.getVersion().then((v:string) =>
      {
        this.versionGetAppReady = v;
      });
    });*/
    platform.ready().then(() => 
    {
      this.getVersion().then((c:ICurrentConfig) =>
      {
        this.buildID = c.currentBuildId;
        this.binaryVersion = c.binaryVersion;
      });
    });
  }


  async getVersion():Promise<ICurrentConfig>{
      console.info("About to get version")
      var config=null;
      try{
        config = await Pro.deploy.getConfiguration();//.getCurrentVersion();
        console.info(config)
      }
      catch(error){
        console.warn("Error during getVersion");
        console.warn(error);
      }
      return config;
  }
}
